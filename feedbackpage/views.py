from django.shortcuts import render
from .forms import Input_Form

# Create your views here.
def feedback(request):
    response = {'input_form' : Input_Form}
    return render(request, 'feedbackpage/feedbackpage.html', response)

def saveFeedback(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return render(request,'feedbackpage/saveFeedback.html')
    else:
        form = Input_Form() 
        return render(request,'feedbackpage/feedbackpage.html') 