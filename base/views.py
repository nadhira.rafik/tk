from django.shortcuts import render, redirect
from .models import PoolingBarang

def index(request):
    likesMasker = PoolingBarang.objects.get(barang = 'Masker').like
    likesSanitizer = PoolingBarang.objects.get(barang = 'Sanitizer').like
    likesFaceshield = PoolingBarang.objects.get(barang = 'Face Shield').like

    return render(request, 'base/index.html', {'likesMasker': likesMasker,'likesSanitizer': likesSanitizer ,'likesFaceshield': likesFaceshield})

def masker(request):
    if request.method =='POST':
        barang = PoolingBarang.objects.get(barang = 'Masker')
        barang.like +=1
        barang.save()
        return redirect ('/')

    return render (request, 'base/index.html')

def sanitizer(request):
    if request.method =='POST':
        barang = PoolingBarang.objects.get(barang = 'Sanitizer')
        barang.like +=1
        barang.save()
        return redirect ('/')

    return render (request, 'base/index.html')

def faceshield(request):
    if request.method =='POST':
        barang = PoolingBarang.objects.get(barang = 'Face Shield')
        barang.like +=1
        barang.save()
        return redirect ('/')

    return render (request, 'base/index.html')