
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('base.urls')),
    path('', include('postingan.urls')),
    path('admin/', admin.site.urls),
    path('feedbackpage/', include('feedbackpage.urls')),
    path('semangatpage/', include('semangatpage.urls')),
    path('profile/', include('profilepage.urls')),
    # path('', include('pooling.urls')),
    # path('', include('postingan.urls')),
    path('jadwal/', include('jadwal.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

