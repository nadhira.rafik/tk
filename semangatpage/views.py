from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import kataForm
from .models import kata

def semangat(request):
    semangat = kata.objects.all()
    return render(request, 'semangatpage/semangat.html', {'semangat' : semangat})

def add_semangat(request):
    if request.method == 'POST':
        form = kataForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/semangatpage/')    
    else:
        form = kataForm()
        return render(request, 'semangatpage/addsemangat.html', {'form': form})