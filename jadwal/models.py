from django.db import models
# Create your models here.
KRITERIA = ( 
    ("Non Critical", "Non Critical"),
    ("Critical", "Critical"), 
)

# ini buat detail jadwal yang ada
class Kriteria(models.Model):
    kriteria = models.CharField ('Kriteria', choices = KRITERIA, default='Non Critical', max_length = 20, null = True)
    def __str__(self):
        return self.kriteria

class Jadwal(models.Model):
    kriteria = models.ForeignKey(Kriteria, on_delete=models.CASCADE, null=True)
    jadwal = models.CharField('Jadwal', max_length=80, null=True)

    def __str__(self):
        return self.jadwal


