# Generated by Django 3.1 on 2020-11-15 04:54

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gambar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_gambar', models.ImageField(upload_to=django.core.files.storage.FileSystemStorage(location='/static/images'))),
                ('judul', models.CharField(max_length=50)),
                ('deskripsi', models.TextField(max_length=500)),
            ],
        ),
    ]
