import os
from covid_web_tk1.settings import *
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from .views import *
from .models import *
from .forms import *
from .apps import PostinganConfig
from django.core.files.uploadedfile import SimpleUploadedFile



# Create your tests here.

class ModelTest(TestCase):
    def setUp(self):
        self.gambar = Gambar(file_gambar="https://www.python.org/static/opengraph-icon-200x200.png",
                                judul = "python",
                                deskripsi="Ini gambar")
        self.gambar.save()
    
    def test_created(self):
        self.assertEqual(Gambar.objects.all().count(), 1)

    def test_str(self):
        self.assertEqual(str(self.gambar), "python")

class FormTest(TestCase):
    def test_form_is_valid(self):
        self.image_file = open(
            os.path.join(BASE_DIR, 'postingan/static/postingan/images/testpic.jpg'), "rb"
        )
        data = {    
            'judul': 'TEST',
            'deskripsi' : 'TESTING DESC'
        }
        files_data = {
            'file_gambar': SimpleUploadedFile(
                self.image_file.name,
                self.image_file.read()
            )
        }
        form = FormGambar(data=data, files=files_data)
        self.assertTrue(form.is_valid())

    def test_form_is_invalid(self):
        form_gambar = FormGambar(data={})
        self.assertFalse(form_gambar.is_valid())

class UrlsTest(TestCase): 
    def setUp(self):
        self.gambar = Gambar(file_gambar="https://www.python.org/static/opengraph-icon-200x200.png",
                                judul = "python",
                                deskripsi="Ini gambar")
        self.gambar.save()

        self.index = reverse("index")
        self.uploadGambar = reverse("uploadGambar")
        self.detail = reverse("detail", args=[self.gambar.id])
    
    def test_func_index(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_func_uploadGambar(self):
        found = resolve(self.uploadGambar)
        self.assertEqual(found.func, upload)

    def test_func_detail(self):
        found = resolve(self.detail)
        self.assertEqual(found.func, detail)
    
    

class ViewsTest(TestCase):  
    def setUp(self):
        self.gambar = Gambar(file_gambar="https://www.python.org/static/opengraph-icon-200x200.png",
                                judul = "python",
                                deskripsi="Ini gambar")
        self.gambar.save()
        self.index = reverse('index')
        self.uploadGambar = reverse("uploadGambar")
        self.detail = reverse("detail", args=[self.gambar.id])

    def test_GET_index (self):
        response = Client().get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'postingan/show.html')
    
    def test_GET_uploadGambar(self):
        response = Client().get(self.uploadGambar)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'postingan/upload.html')

    def test_POST_uploadGambar(self):
        self.image_file = open(
            os.path.join(BASE_DIR, 'postingan/static/postingan/images/testpic.jpg'), "rb"
        )
        response = Client().post(self.uploadGambar,
                                {
                                    "file_gambar":SimpleUploadedFile(
                                        self.image_file.name,
                                        self.image_file.read()
                                    ),
                                    "judul" : "python",
                                    "deskripsi":"ini gambar"
                                }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_uploadGambar_invalid(self):
        response = Client().post(self.uploadGambar,
                                {
                                    "file_gambar":"",
                                    "judul" : "",
                                    "deskripsi":""
                                }, follow=True)
        self.assertTemplateUsed(response, 'postingan/upload.html')

    def test_GET_detail(self):
        response = Client().get(self.detail)                   
        self.assertTemplateUsed(response, 'postingan/detail.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(PostinganConfig.name, 'postingan')
        self.assertEqual(apps.get_app_config('postingan').name, 'postingan')

    





    
