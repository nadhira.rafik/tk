from django import forms
from .models import Gambar
from django.forms import TextInput, Textarea, ImageField

class FormGambar(forms.ModelForm):
    judul = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control-lg',
            'placeholder':'Tulis judul di sini...'
            }
    ))
    deskripsi = forms.CharField(widget=forms.Textarea(
        attrs={
            'class':'form-control',
            'placeholder':'Tulis deskripsi foto di sini...'
        }
    ))
    file_gambar = forms.ImageField()

    class Meta:
        model = Gambar
        fields = ('judul', 'deskripsi', 'file_gambar')